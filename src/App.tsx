import './styles/styles.scss';
import React from 'react';
import { Articulo, ArticuloAutor, ArticuloBajada } from './models/articulo';
import { ArticuloContainer } from './components/ArticuloContainer';
import { FocalIzquierdo } from './components/FocalIzquierdo';

const articulos: Array<Articulo | ArticuloAutor | ArticuloBajada> = [
    {
        id: 1,
        url: '/articulo/1',
        lead: 'Volanta.',
        titulo: 'Esto es un titulo de la novedad con unas cuantas lineas que ocupar, esto es un titulo de la novedad',
        image: {
            url: '/assets/imagen_externa.png',
            alt: 'Imagen generica'
        },
        marquee: 'Marquee',
    },
    {
        id: 2,
        url: '/articulo/2',
        lead: 'Lead 2.',
        titulo: 'Title esto es un titulo de la novedad con unas cuantas lineas que ocupar.',
        bajada: 'Lana soñaba con volar a la luna. Todas las tardes se tumbaba en su cama y se imaginaba cómo sería su viaje a bordo de su propia nave espacial.',
        image: {
            url: '/assets/imagen_externa.png',
            alt: 'Imagen generica'
        },
        marquee: 'Marquee'
    },
    {
        id: 3,
        url: '/articulo/3',
        lead: 'Lead 3.',
        titulo: 'Title Cuando se dieron cuenta, los dos hermanos estaban preparándose para alunizar.',
        author: {
            name: 'Autor Full Name',
            image_url: '/assets/imagen_autor.png',
        }
    },
    {
        id: 4,
        url: '/articulo/4',
        lead: 'Lead 4.',
        titulo: 'Esto es un titulo de la novedad con unas cuantas lineas que ocupar.',
        image: {
            url: '/assets/imagen_externa.png',
            alt: 'Imagen generica'
        }
    },
    {
        id: 5,
        url: '/articulo/5',
        lead: 'Lead 5.',
        titulo: 'Esto es un titulo',
        image: {
            url: '/assets/imagen_externa.png',
            alt: 'Imagen generica'
        },
    },
    {
        id: 6,
        url: '/articulo/6',
        lead: 'Lead 6.',
        titulo: 'Esto es un titulo de la novedad con unas cuantas lineas que ocupar.',
        image: {
            url: '/assets/imagen_externa.png',
            alt: 'Imagen generica'
        },
        marquee: 'Marquesina 1',
    },
]

const articulosFocalIzquierdoPrincipal: Required<Articulo> = {
    id: 7,
    url: '/articulo/7',
    lead: 'Lead.',
    titulo: 'Title Cuando se dieron cuenta, los dos hermanos estaban preparándose para alunizar.',
    bajada: 'Subhead. Lana soñaba con volar a la luna. Todas las tardes se tumbaba en su cama y se imaginaba cómo sería su viaje a bordo de su propia nave espacial.',
    image: {
        url: '/assets/imagen_externa.png',
        alt: 'Imagen generica'
    },
    author: {
        name: 'Autor',
        image_url: '/assets/imagen_autor.png',
    },
    marquee: 'Marquee'
};

const articulosFocalIzquierdoSecundarios: ArticuloAutor[] = [
    {
        id: 8,
        url: '/articulo/8',
        lead: 'Lead.',
        titulo: 'Title esto es un titulo de la novedad con unas cuantas lineas que ocupar',
        image: {
            url: '/assets/imagen_externa.png',
            alt: 'Imagen generica'
        },
        author: {
            name: 'Autor',
            image_url: '/assets/imagen_autor.png',
        },
        marquee: 'Marquee'
    },
    {
        id: 9,
        url: '/articulo/9',
        lead: 'Lead.',
        titulo: 'Title esto es un titulo de la novedad con unas cuantas lineas que ocupar',
        image: {
            url: '/assets/imagen_externa.png',
            alt: 'Imagen generica'
        },
        author: {
            name: 'Autor',
            image_url: '/assets/imagen_autor.png',
        },
        marquee: 'Marquee'
    },
];


function App() {
    return (
        <div className="App">
            <FocalIzquierdo
                titulo="Focal Izquiedo"
                articuloPrincipal={articulosFocalIzquierdoPrincipal}
                articulosSide={articulosFocalIzquierdoSecundarios}
            />
            <ArticuloContainer
                titulo="Notas x 3, 6, 9, 12"
                link="https://www.lanacion.com.ar/"
                articulos={articulos}
            />
        </div>
    );
}

export default App;
