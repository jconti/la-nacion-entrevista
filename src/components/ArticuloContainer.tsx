import { Articulo } from '../models/articulo';
import React, { Fragment } from 'react';
import { ArticuloRegular } from './ArticuloRegular';
import './ArticuloContainer.scss';

interface ArticulosContainerProps {
    articulos: Articulo[];
    titulo?: string;
    link?: string;
}

export function ArticuloContainer({ articulos, titulo, link }: ArticulosContainerProps) {
    return (
        <section className="seccion seccion-articulos">
            {titulo &&
                <Fragment>
                    {
                        link
                        ?
                            <a
                                className="seccion-articulos__titulo seccion-articulos__link"
                                href={link}
                                target="_blank"
                                rel="noreferrer"
                            >{titulo}</a>
                        : <h3 className="seccion-articulos__titulo">{titulo}</h3>
                    }

                    <hr className="separador"/>
                </Fragment>
            }
            <div className="seccion-articulos__contenedor">
                {articulos.map((articulo) =>
                    <ArticuloRegular
                        autor={articulo?.author}
                        key={articulo.id}
                        articulo={articulo}
                        size={'sm'}
                    />
                )}
            </div>
        </section>
    );
}
