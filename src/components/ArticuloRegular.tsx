import React from 'react';
import { Articulo, Autor } from '../models/articulo';

interface ArticuloProps {
    articulo: Articulo;
    autor?: Autor;
    esInvertido?: boolean;
    size?: 'sm' | 'lg';
}

export function ArticuloRegular({ articulo, autor, size, esInvertido }: ArticuloProps) {
    let articuloClassName = 'articulo';

    if (autor) {
        articuloClassName += ' articulo--autor';
    } else if (articulo.bajada && !esInvertido) {
        articuloClassName += ' articulo--bajada';
    }

    if (size) {
        articuloClassName += ` articulo--${size}`;
    }

    if (esInvertido) {
        articuloClassName += ` articulo--invertido`;
    }

    return (
        <article className={articuloClassName}>
            {articulo.image &&
                <div className="articulo__image articulo-image">
                    <img
                        className="articulo-image__image"
                        src={articulo.image.url}
                        alt={articulo.image.alt}
                    />
                </div>
            }

            <div className="articulo__body articulo-body">
                <h5 className="articulo-body__titulo"><span className="articulo-body__lead">{articulo.lead}</span> {articulo.titulo}</h5>
                {articulo.bajada &&
                    <p className="articulo-body__bajada">{articulo.bajada}</p>
                }
                {articulo.marquee &&
                    <small className="articulo-body__small">{articulo.marquee}{articulo.author?.name && ` / ${articulo.author.name}`}</small>
                }
                {autor &&
                    <div className="articulo-body__autor articulo-autor">
                        <div
                            className="articulo-autor__image"
                            role="img"
                            aria-label={autor.name}
                            title="Imagen de autor"
                            style={ {backgroundImage: `url(${autor.image_url})` }}
                        />
                        <p className="articulo-autor__name">{autor.name}</p>
                    </div>
                }
            </div>
        </article>
    );
}
