import React, { Fragment } from 'react';
import { Articulo } from '../models/articulo';
import { ArticuloRegular } from './ArticuloRegular';
import './FocoIzquierdo.scss';

interface FocalIzquierdoProps {
    articuloPrincipal: Articulo;
    articulosSide: Articulo[];
    titulo?: string;
    link?: string;
}

export function FocalIzquierdo({ articuloPrincipal, articulosSide, titulo, link }: FocalIzquierdoProps ) {
    return (
        <section className="seccion seccion-focal-izquierdo">
            {titulo &&
            <Fragment>
                {
                    link
                        ?
                        <a
                            className="seccion-focal-izquierdo__titulo seccion-focal-izquierdo__link"
                            href={link}
                            target="_blank"
                            rel="noreferrer"
                        >{titulo}</a>
                        : <h3 className="seccion-focal-izquierdo__titulo">{titulo}</h3>
                }

                <hr className="separador"/>
            </Fragment>
            }
            <div className="seccion-focal-izquierdo__contenedor">
                <div className="seccion-focal-izquierdo__principal">
                    <ArticuloRegular
                        articulo={articuloPrincipal}
                        esInvertido={true}
                    />
                </div>
                <div className="seccion-focal-izquierdo__secundario">
                    {articulosSide.map(articulo =>
                        <ArticuloRegular
                            key={articulo.id}
                            articulo={articulo}
                            size={'sm'}
                        />
                    )}
                </div>
            </div>
        </section>
    );
}
