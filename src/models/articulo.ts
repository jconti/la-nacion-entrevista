export interface Imagen {
    url: string;
    alt: string;
}

export interface Autor {
    name: string;
    image_url: string;
}

export interface Articulo {
    id: number;
    url: string;
    lead: string;
    titulo: string;
    image?: Imagen;
    bajada?: string;
    marquee?: string;
    author?: Autor;
}

export interface ArticuloAutor extends Articulo {
    author: Autor;
}

export interface ArticuloBajada extends Articulo {
    bajada: string;
}
